#!/bin/bash

set -e

#set to false to not see all the echos
#debug=true

# Set up the virtual environment and install dependencies.
python3 -m venv alphafold_venv
source alphafold_venv/Scripts/activate
pip install wheel
pip install -r requirements.txt

DISTOGRAM_MODEL="alphafold_casp13/models/873731"
BACKGROUND_MODEL="alphafold_casp13/models/916425"
TORSION_MODEL="alphafold_casp13/models/941521"

TARGET_FOLDER="alphafold_casp13/targets/*"

for file in $TARGET_FOLDER
do
  TARGET="$(basename "$file")"  # The name of the target.
  TARGET_PATH="alphafold_casp13/targets/${TARGET}"  # Path to the directory with the target input data.

  # Create the output directory.
  OUTPUT_DIR="contact/contacts_${TARGET}_$(date +%Y_%m_%d_%H_%M_%S)"
  mkdir -p "${OUTPUT_DIR}"
  echo "Saving output to ${OUTPUT_DIR}/"

  # Run contact prediction over 4 replicas.
  for replica in 0 1 2 3; do
    echo "Launching all models for replica ${replica}"

    # Run the distogram model.
    python3 -m contacts \
      --logtostderr \
      --cpu=true \
      --config_path="${DISTOGRAM_MODEL}/${replica}/config.json" \
      --checkpoint_path="${DISTOGRAM_MODEL}/${replica}/tf_graph_data/tf_graph_data.ckpt" \
      --output_path="${OUTPUT_DIR}/distogram/${replica}" \
      --eval_sstable="${TARGET_PATH}/${TARGET}.tfrec" \
      --stats_file="${DISTOGRAM_MODEL}/stats_train_s35.json" &

    # Run the background model.
    python3 -m contacts \
      --logtostderr \
      --cpu=true \
      --config_path="${BACKGROUND_MODEL}/${replica}/config.json" \
      --checkpoint_path="${BACKGROUND_MODEL}/${replica}/tf_graph_data/tf_graph_data.ckpt" \
      --output_path="${OUTPUT_DIR}/background_distogram/${replica}" \
      --eval_sstable="${TARGET_PATH}/${TARGET}.tfrec" \
      --stats_file="${BACKGROUND_MODEL}/stats_train_s35.json" &
  done

  # Run the torsion model, but only 1 replica.
  python3 -m contacts \
    --logtostderr \
    --cpu=true \
    --config_path="${TORSION_MODEL}/0/config.json" \
    --checkpoint_path="${TORSION_MODEL}/0/tf_graph_data/tf_graph_data.ckpt" \
    --output_path="${OUTPUT_DIR}/torsion/0" \
    --eval_sstable="${TARGET_PATH}/${TARGET}.tfrec" \
    --stats_file="${TORSION_MODEL}/stats_train_s35.json" &

  echo "All models running, waiting for them to complete"
  wait

  echo "Ensembling all replica outputs"

  # Run the ensembling jobs for distograms, background distograms.
  for output_dir in "${OUTPUT_DIR}/distogram" "${OUTPUT_DIR}/background_distogram"; do
    pickle_dirs="${output_dir}/0/pickle_files/,${output_dir}/1/pickle_files/,${output_dir}/2/pickle_files/,${output_dir}/3/pickle_files/"

    # Ensemble distograms.
    python3 -m ensemble_contact_maps \
      --logtostderr \
      --pickle_dirs="${pickle_dirs}" \
      --output_dir="${output_dir}/ensemble/"
  done

  # Only ensemble single replica distogram for torsions.
  python3 -m ensemble_contact_maps \
    --logtostderr \
    --pickle_dirs="${OUTPUT_DIR}/torsion/0/pickle_files/" \
    --output_dir="${OUTPUT_DIR}/torsion/ensemble/"

  echo "Pasting contact maps"

  python3 -m paste_contact_maps \
    --logtostderr \
    --pickle_input_dir="${OUTPUT_DIR}/distogram/ensemble/" \
    --output_dir="${OUTPUT_DIR}/pasted/" \
    --tfrecord_path="${TARGET_PATH}/${TARGET}.tfrec"

  echo "Done"

done